package com.sdata.ecommerce.domain;

import lombok.Data;

/**
 * @author nedli
 */
@Data
public class BaseRequest {
    private Integer pageSize;
    private Integer pageNum;
    private String keyword;
}
