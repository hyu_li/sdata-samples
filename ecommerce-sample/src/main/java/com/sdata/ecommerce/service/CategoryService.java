package com.sdata.ecommerce.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sdata.ecommerce.domain.BaseRequest;
import com.sdata.ecommerce.domain.Category;
import com.sdata.ecommerce.domain.CategoryExample;
import com.sdata.ecommerce.domain.SearchCategoryResponse;
import com.sdata.ecommerce.mapper.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author nedli
 */
@Service
public class CategoryService {
    @Autowired
    private CategoryMapper categoryMapper;

    public Category getCategoryDetailById(String id) {
        return categoryMapper.getCategoryDetailById(id);
    }


    public SearchCategoryResponse search(BaseRequest request) {
        PageHelper.startPage(request.getPageNum(), request.getPageSize());
        CategoryExample example = new CategoryExample();

        if (request.getKeyword() != null && !request.getKeyword().equals("")) {
            example.createCriteria().andNameLike("%" + request.getKeyword() + "%");
        }

        List<Category> categories = categoryMapper.selectByExample(example);

        return new SearchCategoryResponse(categories, ((Page) categories).getTotal());
    }

    public int saveOrUpdateCategory(Category category) {
        if (Objects.nonNull(category.getId())) {
            return categoryMapper.updateByPrimaryKey(category);
        } else {
            return categoryMapper.insert(category);
        }
    }

    public int saveCategory(Category category) {
        return categoryMapper.insert(category);
    }

    public Object getCategoryById(String id) {
        return categoryMapper.selectByPrimaryKey(id);
    }
}
