package com.sdata.ecommerce.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sdata.ecommerce.domain.Product;
import com.sdata.ecommerce.domain.ProductExample;
import com.sdata.ecommerce.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author nedli
 */
@Service
public class ProductService {
    @Autowired
    private ProductMapper productMapper;

    public List<Product> listProdcuts(){
        return productMapper.selectByExample(new ProductExample());
    }

    public List<Product> search(String name) {
        PageHelper.startPage(1, 5);
        ProductExample example = new ProductExample();
        example.createCriteria().andNameLike("%" + name + "%");

        List<Product> products =  productMapper.selectByExample(example);

        System.out.println(((Page)products).getTotal());

        return products;
    }
}
