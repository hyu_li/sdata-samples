package com.sdata.ecommerce.controller;

import com.sdata.ecommerce.domain.Category;
import com.sdata.ecommerce.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author nedli
 */
@Controller
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping
    public String listPage() {
        return "/pages/catalog/category-list.html";
    }


    @GetMapping("/add")
    public String addPage(ModelMap model) {
        model.addAttribute("category", new Category());
        return "/pages/catalog/category-edit";
    }

    @GetMapping("/edit")
    public String editPage(ModelMap model, @RequestParam("id") String id) {
        model.addAttribute("category", categoryService.getCategoryById(id));
        return "/pages/catalog/category-edit";
    }

    @PostMapping("/save")
    public String saveCategory(Category category) {
        categoryService.saveOrUpdateCategory(category);

        return "redirect:/category";
    }
}
