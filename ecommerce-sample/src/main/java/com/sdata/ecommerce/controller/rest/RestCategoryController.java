package com.sdata.ecommerce.controller.rest;

import com.sdata.ecommerce.domain.BaseRequest;
import com.sdata.ecommerce.domain.Category;
import com.sdata.ecommerce.domain.SearchCategoryResponse;
import com.sdata.ecommerce.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author nedli
 */
@RestController
@RequestMapping("/api/v1/categories")
public class RestCategoryController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping("/search")
    public SearchCategoryResponse search(BaseRequest request) {
        return categoryService.search(request);
    }
 }
