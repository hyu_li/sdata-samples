package com.sdata.ecommerce.mapper.extended;

import com.sdata.ecommerce.domain.Category;

/**
 * @author nedli
 */
public interface CategoryMapperExtended {
    Category getCategoryDetailById(String id);
}
