package com.sdata.ecommerce.service;

import com.sdata.ecommerce.domain.Category;
import com.sdata.ecommerce.domain.Product;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author nedli
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryServiceTest {
    @Autowired
    private CategoryService categoryService;

    @Test
    public void testGetCategoryById(){
        Category category = categoryService.getCategoryDetailById("2");
        Assert.assertNotNull(category);
    }
}
