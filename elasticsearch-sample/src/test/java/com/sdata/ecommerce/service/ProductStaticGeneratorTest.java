package com.sdata.ecommerce.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author nedli
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductStaticGeneratorTest {
    @Autowired
    private ProductStaticGenerator staticGenerator;

    @Test
    public void generateTest() {
        staticGenerator.generate();
    }
}
