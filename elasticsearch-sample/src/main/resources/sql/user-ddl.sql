DROP TABLE IF EXISTS sys_user;
CREATE TABLE sys_user (
  id           VARCHAR(36) COMMENT '自增ID' PRIMARY KEY,
  name         VARCHAR(50) COMMENT '账号名' UNIQUE,
  nick_name    VARCHAR(60) COMMENT '昵称',
  password     VARCHAR(200) COMMENT '密码',
  email        VARCHAR(200) COMMENT '邮箱',
  phone        VARCHAR(30) COMMENT '手机号',
  enabled      BOOLEAN COMMENT '是否启用账号' DEFAULT TRUE,
  created_date TIMESTAMP COMMENT '创建时间' DEFAULT current_timestamp
);

DROP TABLE IF EXISTS sys_role;
CREATE TABLE sys_role (
  id           VARCHAR(36) COMMENT '唯一ID' PRIMARY KEY,
  name         VARCHAR(200) COMMENT '角色名称',
  created_date TIMESTAMP COMMENT '创建时间'
);

DROP TABLE IF EXISTS sys_user_role;
CREATE TABLE sys_user_role (
  id      VARCHAR(36) COMMENT '唯一ID' PRIMARY KEY,
  user_id VARCHAR(36) COMMENT '用户ID',
  role_id VARCHAR(36) COMMENT '角色ID',
  CONSTRAINT `fk_sys_user_role_user_id` FOREIGN KEY (user_id) REFERENCES sys_user (id),
  CONSTRAINT `fk_sys_user_role_role_id` FOREIGN KEY (role_id) REFERENCES sys_role (id),
  UNIQUE KEY unique_sys_user_role (user_id, role_id)
);

DROP TABLE IF EXISTS sys_permission;
CREATE TABLE sys_permission (
  id           VARCHAR(36) COMMENT '唯一ID' PRIMARY KEY,
  name         VARCHAR(200) COMMENT '名称'    NOT NULL,
  url          varchar(600) COMMENT 'URL地址' NOT NULL,
  category     varchar(60) COMMENT '类别'     NOT NULL,
  description  VARCHAR(300) COMMENT '描述'    NOT NULL,
  created_date TIMESTAMP
);

DROP TABLE IF EXISTS sys_role_permission;
CREATE TABLE sys_role_permission (
  id            VARCHAR(36) PRIMARY KEY,
  role_id       VARCHAR(36),
  permission_id VARCHAR(36),
  CONSTRAINT `fk_sys_role_permission_user_id` FOREIGN KEY (role_id) REFERENCES sys_role (id),
  CONSTRAINT `fk_sys_role_permission_role_id` FOREIGN KEY (permission_id) REFERENCES sys_permission (id),
  UNIQUE KEY unique_sys_role_permission (role_id, permission_id)
);