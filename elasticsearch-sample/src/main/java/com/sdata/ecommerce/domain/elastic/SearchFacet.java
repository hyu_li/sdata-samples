package com.sdata.ecommerce.domain.elastic;

import lombok.Data;

import java.util.List;

/**
 * @author nedli
 */
@Data
public class SearchFacet {
    private String abbreviation;
    private List<SearchFacetResult> facetValues;
}
