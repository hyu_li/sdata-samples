package com.sdata.ecommerce.domain;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author nedli
 */
public class SystemUser extends User implements UserDetails {
    public SystemUser() {
    }

    public SystemUser(String id, String name, String nickName,
                      String password, String email, String phone, Byte enabled, Date createdDate) {
        super(id, name, nickName, password, email, phone, enabled, createdDate);
    }

    public SystemUser(User user) {
        super(user.getId(), user.getName(), user.getNickName(), user.getPassword(), user.getEmail(), user.getPhone(), user.getEnabled(), user.getCreatedDate());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();

        List<Role> userRoles = this.getRoles();

        if (userRoles != null) {
            for (Role role : userRoles) {
                if (role != null && StringUtils.isNotBlank(role.getName())) {
                    SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role.getName());
                    authorities.add(authority);
                }
            }
        }

        return authorities;
    }

    @Override
    public String getUsername() {
        return super.getName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
