package com.sdata.ecommerce.domain.elastic;

import lombok.Data;
import org.elasticsearch.action.search.SearchResponse;

import java.util.List;

/**
 * @author nedli
 */
@Data
public class SearchResult {
    private List<SearchProduct> products;
    private List<SearchFacet> facets;

    protected Long total;
    protected Integer page;
    protected Integer pageSize;

    protected SearchResponse searchResponse;
}
