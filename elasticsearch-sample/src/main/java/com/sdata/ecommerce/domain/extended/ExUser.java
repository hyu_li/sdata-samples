package com.sdata.ecommerce.domain.extended;

import com.sdata.ecommerce.domain.Role;

import java.util.List;

/**
 * @author nedli
 */
public class ExUser {
    private List<Role> roles;

    /**
     * Gets roles
     *
     * @return value of roles
     */
    public List<Role> getRoles() {
        return roles;
    }

    /**
     * Sets roles
     */
    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
