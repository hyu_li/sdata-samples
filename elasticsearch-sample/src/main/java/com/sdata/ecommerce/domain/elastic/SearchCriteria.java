package com.sdata.ecommerce.domain.elastic;

import com.sdata.ecommerce.domain.Category;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author nedli
 */
@Data
public class SearchCriteria {
    public static String PAGE_SIZE_STRING = "pageSize";
    public static String PAGE_NUMBER = "page";
    public static String SORT_STRING = "sort";
    public static String QUERY = "q";
    public static String SORT_SEPARATOR = "::";

    private Integer page;
    private Integer pageSize;
    private Category category;
    private String sortField;
    private String sortOrder;
    private String query;
    protected Map<String, String[]> filterCriteria = new HashMap<>();

    public Integer getFrom() {
        return pageSize * (page - 1);
    }
}
