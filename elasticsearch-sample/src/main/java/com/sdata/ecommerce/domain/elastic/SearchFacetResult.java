package com.sdata.ecommerce.domain.elastic;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author nedli
 */
@Data
public class SearchFacetResult {
    private String value;

    private BigDecimal minValue;
    private BigDecimal maxValue;

    private Long quantity;
}
