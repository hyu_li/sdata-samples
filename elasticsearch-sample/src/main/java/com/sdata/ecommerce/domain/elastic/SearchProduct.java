package com.sdata.ecommerce.domain.elastic;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author nedli
 */
@Data
@NoArgsConstructor
public class SearchProduct {
    private String id;
    private String name;
    private String description;
    private String keywords;
    private String parentCategory;
    private String manufacturer;
    private Double msrp;
    private Double salePrice;
    private String template;
}
