package com.sdata.ecommerce.service;

import org.elasticsearch.action.bulk.BulkResponse;

/**
 * @author nedli
 */
public interface IndexService {
    BulkResponse index();
}
