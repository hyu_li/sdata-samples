package com.sdata.ecommerce.service;

import com.sdata.ecommerce.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * @author nedli
 */
@Service
public class ProductStaticGeneratorImpl implements ProductStaticGenerator {
    @Autowired
    private ProductService productService;

    @Autowired
    private TemplateEngine templateEngine;

    @Override
    public int generate() {
        int total = 0;

        List<Product> products = productService.listAllProducts();

        for (int i = 0; i < 1; i++) {
            Product product = products.get(i);
            Context context = new Context();
            context.setVariable("product", product);

            try {
                File file = new File("src/main/resources/public/product/" + product.getId() + ".html");
                FileWriter  writer = new FileWriter(file);
                templateEngine.process("/catalog/default-product-detail.html", context, writer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return total;
    }
}
