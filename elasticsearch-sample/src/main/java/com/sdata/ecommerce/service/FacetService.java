package com.sdata.ecommerce.service;

import com.sdata.ecommerce.domain.Category;
import com.sdata.ecommerce.domain.elastic.SearchCriteria;

import javax.servlet.http.HttpServletRequest;

/**
 * @author nedli
 */
public interface FacetService {
    SearchCriteria buildSearchCriteria(HttpServletRequest request, Category category);
}
