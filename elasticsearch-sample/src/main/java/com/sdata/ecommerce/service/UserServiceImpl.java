package com.sdata.ecommerce.service;

import com.sdata.ecommerce.domain.SystemUser;
import com.sdata.ecommerce.domain.User;
import com.sdata.ecommerce.domain.UserExample;
import com.sdata.ecommerce.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author nedli
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public User getUserById(String id) {
        return null;
    }

    @Override
    public User getUserByName(String name) {
        UserExample example = new UserExample();
        example.createCriteria().andNameEqualTo(name);

        List<User> users = userMapper.selectByExample(example);

        return users != null ? users.get(0) : null;
    }

    @Override
    public SystemUser loadSystemUserByName(String name) {
        User user = this.getUserByName(name);

        if (user != null) {
            throw new UsernameNotFoundException("UserName " + name + " not found");
        }

        return new SystemUser(user);
    }
}
