package com.sdata.ecommerce.service;

import com.sdata.ecommerce.domain.Category;

/**
 * @author nedli
 */
public interface CategoryService {
    Category findCategoryByName(String name);
}
