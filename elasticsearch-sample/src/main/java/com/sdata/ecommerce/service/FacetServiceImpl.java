package com.sdata.ecommerce.service;

import com.sdata.ecommerce.domain.Category;
import com.sdata.ecommerce.domain.elastic.SearchCriteria;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.sdata.ecommerce.domain.elastic.SearchCriteria.SORT_SEPARATOR;
import static org.apache.commons.lang3.StringUtils.substringAfterLast;
import static org.apache.commons.lang3.StringUtils.substringBeforeLast;

/**
 * @author nedli
 */
@Service
public class FacetServiceImpl implements FacetService {
    @Autowired
    private ExploitProtectionService exploitProtectionService;

    @Override
    public SearchCriteria buildSearchCriteria(HttpServletRequest request, Category category) {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setPageSize(getDefaultPageSize());
        searchCriteria.setPage(getDefaultPage());

        Map<String, String[]> facets = new HashMap<>();
        for (Map.Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
            String key = entry.getKey();
            if (Objects.equals(key, SearchCriteria.SORT_STRING)) {
                String sort = entry.getValue()[0];
                searchCriteria.setSortField(substringBeforeLast(sort, SORT_SEPARATOR));
                searchCriteria.setSortOrder(substringAfterLast(sort, SORT_SEPARATOR));
            } else if (Objects.equals(key, SearchCriteria.PAGE_SIZE_STRING)) {
                searchCriteria.setPageSize(Integer.parseInt(entry.getValue()[0]));
            } else if (Objects.equals(key, SearchCriteria.QUERY)) {
                String query = entry.getValue()[0];
                if (StringUtils.isNotEmpty(query)) {
                    query = exploitProtectionService.cleanString(StringUtils.trim(query));
                }
                searchCriteria.setQuery(query);
            } else {
                facets.put(key, entry.getValue());
            }
        }
        searchCriteria.setFilterCriteria(facets);
        searchCriteria.setCategory(category);

        return searchCriteria;
    }

    protected Integer getDefaultPageSize() {
        return 20;
    }

    protected Integer getDefaultPage() {
        return 1;
    }
}
