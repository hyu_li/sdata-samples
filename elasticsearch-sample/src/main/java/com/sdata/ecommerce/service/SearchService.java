package com.sdata.ecommerce.service;

import com.sdata.ecommerce.domain.elastic.SearchCriteria;
import com.sdata.ecommerce.domain.elastic.SearchResult;

/**
 * @author nedli
 */
public interface SearchService {
    SearchResult search(SearchCriteria searchCriteria);
}
