package com.sdata.ecommerce.service;

import com.sdata.ecommerce.domain.Category;
import com.sdata.ecommerce.domain.CategoryExample;
import com.sdata.ecommerce.mapper.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author nedli
 */
@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public Category findCategoryByName(String name) {
        CategoryExample example = new CategoryExample();
        example.createCriteria().andNameEqualTo(name);

        List<Category> categories = categoryMapper.selectByExample(example);

        return categories.size() > 0 ? categories.get(0) : null;
    }
}
