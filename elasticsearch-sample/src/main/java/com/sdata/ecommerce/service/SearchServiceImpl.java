package com.sdata.ecommerce.service;

import com.sdata.ecommerce.domain.elastic.*;
import com.sdata.ecommerce.elasticsearch.tool.ElasticsearchTool;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.sdata.ecommerce.util.JacksonMapper.mapper;

/**
 * @author nedli
 */
@Service
public class SearchServiceImpl implements SearchService {
    @Autowired
    private ElasticsearchTool elasticsearchTool;

    @Override
    public SearchResult search(SearchCriteria searchCriteria) {
        SearchSourceBuilder builder = new SearchSourceBuilder();

        //Pagination.
        builder.size(searchCriteria.getPageSize());
        builder.from(searchCriteria.getFrom());

        //Query builder.
        QueryBuilder queryBuilder = QueryBuilders.matchAllQuery();
        if (StringUtils.isNotBlank(searchCriteria.getQuery())) {
            queryBuilder = QueryBuilders.boolQuery();
            if (StringUtils.isNotBlank(searchCriteria.getQuery())) {
                ((BoolQueryBuilder) queryBuilder).must(QueryBuilders.matchQuery("name", searchCriteria.getQuery()));
            }
//            if (searchCriteria.getCategory() != null) {
//                ((BoolQueryBuilder) queryBuilder).must(QueryBuilders.termQuery("categoryId", searchCriteria.getCategory().getId()));
//            }
        }
        builder.query(queryBuilder);

        //Add sort.
        if (StringUtils.isNotBlank(searchCriteria.getSortField())) {
            builder.sort(searchCriteria.getSortField(), SortOrder.valueOf(searchCriteria.getSortOrder().toUpperCase()));
        }

        //Add aggregation.
        builder.aggregation(AggregationBuilders
                .terms("group_manufacturer")
                .field("manufacturer")
                .size(5)
        );

        SearchRequest searchRequest = new SearchRequest("product").types("_doc").source(builder);
        try {
            SearchResponse response = elasticsearchTool.getClient().search(searchRequest, RequestOptions.DEFAULT);
            SearchResult result = new SearchResult();

            //Pagination.
            result.setPage(searchCriteria.getPage());
            result.setPageSize(searchCriteria.getPageSize());
            result.setTotal(response.getHits().getTotalHits()); //TODO:

            // Records.
            List<SearchProduct> products = new ArrayList<>();

            for (SearchHit searchHit : response.getHits().getHits()) {
                products.add(mapper().readValue(searchHit.getSourceAsString(), SearchProduct.class));
            }
            result.setProducts(products);

            //Facets.
            if (response.getAggregations() != null) {
                List<SearchFacet> facets = new ArrayList<>();
                List<Aggregation> aggs = response.getAggregations().asList();
                for (Aggregation agg : aggs) {
                    SearchFacet searchFacet = new SearchFacet();
                    searchFacet.setAbbreviation(agg.getName());
                    List<SearchFacetResult> results = new ArrayList<>();
                    for (Terms.Bucket bucket : ((ParsedStringTerms) agg).getBuckets()) {
                        SearchFacetResult searchFacetResult = new SearchFacetResult();
                        searchFacetResult.setValue(bucket.getKeyAsString());
                        searchFacetResult.setQuantity(bucket.getDocCount());
                        results.add(searchFacetResult);
                    }
                    searchFacet.setFacetValues(results);
                    facets.add(searchFacet);
                }
                result.setFacets(facets);
            }

            return result;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
