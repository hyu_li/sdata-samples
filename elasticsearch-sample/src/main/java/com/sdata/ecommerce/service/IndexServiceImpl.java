package com.sdata.ecommerce.service;

import com.sdata.ecommerce.domain.Product;
import com.sdata.ecommerce.domain.elastic.SearchProduct;
import com.sdata.ecommerce.elasticsearch.tool.ElasticsearchTool;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.action.bulk.BulkResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author nedli
 */
@Service
public class IndexServiceImpl implements IndexService {
    private static final Log logger = LogFactory.getLog(IndexServiceImpl.class);

    @Autowired
    private ElasticsearchTool elasticsearchTool;
    @Autowired
    private ProductService productService;


    @Override
    public BulkResponse index() {
        //Delete index.
        elasticsearchTool.deleteIndex("product");
        //Create index.
        elasticsearchTool.createIndex("product", readIdxConfig());

        //Map product indexed.
        List<Product> products = productService.listAllProducts();
        List<SearchProduct> searchProducts = mapIdxProducts(products);

        //Bulk insert
        return elasticsearchTool.bulkIndex("product", "_doc", searchProducts);
    }

    protected String readIdxConfig() {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("idx/idx-product.json");
        try {
            return IOUtils.toString(inputStream, Charset.forName("UTF-8"));
        } catch (IOException e) {
            logger.error("Read index config failed. {}", e);
            throw new RuntimeException("Read index config failed.");
        }
    }

    protected List<SearchProduct> mapIdxProducts(List<Product> products) {
        return products.stream().map(productFunc).collect(Collectors.toList());
    }

    Function<Product, SearchProduct> productFunc = (product -> {
        SearchProduct searchProduct = new SearchProduct();
        searchProduct.setId(product.getId());
        searchProduct.setName(product.getName());
        searchProduct.setDescription(product.getDescription());
        searchProduct.setKeywords(product.getKeywords());
        searchProduct.setManufacturer(product.getManufacturer());
        searchProduct.setParentCategory(product.getParentCategory());
        searchProduct.setMsrp(product.getMsrp());
        searchProduct.setSalePrice(product.getSalePrice());
        searchProduct.setTemplate(product.getTemplate());

        return searchProduct;
    }
    );
}
