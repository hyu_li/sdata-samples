package com.sdata.ecommerce.service;

/**
 * @author nedli
 */
public interface ProductStaticGenerator {
    int generate();
}
