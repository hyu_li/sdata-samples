package com.sdata.ecommerce.service;

import com.sdata.ecommerce.domain.SystemUser;
import com.sdata.ecommerce.domain.User;

/**
 * @author nedli
 */
public interface UserService {
    User getUserById(String id);

    User getUserByName(String name);

    SystemUser loadSystemUserByName(String name);
}
