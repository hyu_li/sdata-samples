package com.sdata.ecommerce.controller;

import com.sdata.ecommerce.api.BaseResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.sdata.ecommerce.exception.ErrorCode.AUTH_REQUIED;

/**
 * @author nedli
 */
@Controller
@ResponseStatus(code = HttpStatus.UNAUTHORIZED)
public class LoginController {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @GetMapping("/auth/require")
    @ResponseBody
    public BaseResponse requireAuthentication(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (!request.getRequestURI().startsWith("/api")) {
            response.sendRedirect("/login");
        }

        return new BaseResponse(AUTH_REQUIED);
    }

    @GetMapping("/login")
    public String loginPage() {
        return "/login";
    }
}
