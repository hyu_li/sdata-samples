package com.sdata.ecommerce.controller;

import com.sdata.ecommerce.domain.Category;
import com.sdata.ecommerce.domain.elastic.SearchCriteria;
import com.sdata.ecommerce.domain.elastic.SearchResult;
import com.sdata.ecommerce.service.CategoryService;
import com.sdata.ecommerce.service.FacetService;
import com.sdata.ecommerce.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author nedli
 */
@Controller
public class CategoryController {
    protected static final String DEFAULT_CATEGORY_VIEW = "catalog/category";

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private FacetService facetService;
    @Autowired
    private SearchService searchService;

    @RequestMapping("/category/{categoryName}")
    public String categoryPage(HttpServletRequest request,
                               ModelMap model,
                               @PathVariable("categoryName") String categoryName) {
        Category category = categoryService.findCategoryByName(categoryName);

        //Build facet criteria.
        SearchCriteria searchCriteria = facetService.buildSearchCriteria(request, category);
        //Search by criteria.

        SearchResult result = searchService.search(searchCriteria);

        model.addAttribute("category", category);
        model.addAttribute("products", result.getProducts());
        model.addAttribute("facets", result.getFacets());
        model.addAttribute("total", result.getTotal());
        model.addAttribute("page", result.getPage());
        model.addAttribute("pageSize", result.getPageSize());


        return "/catalog/default-category-list";
    }
}
