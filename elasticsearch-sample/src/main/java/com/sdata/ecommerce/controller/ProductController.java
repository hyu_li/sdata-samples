package com.sdata.ecommerce.controller;

import com.sdata.ecommerce.service.IndexService;
import org.elasticsearch.action.bulk.BulkResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author nedli
 */
@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
    @Autowired
    private IndexService indexService;

    @GetMapping("/index")
    public BulkResponse createIndex() {
        return indexService.index();
    }
}
