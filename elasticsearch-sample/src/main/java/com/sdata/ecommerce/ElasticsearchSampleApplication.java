package com.sdata.ecommerce;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.elasticsearch.rest.RestClientAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = {RestClientAutoConfiguration.class})
@MapperScan("com.sdata.ecommerce.mapper")
public class ElasticsearchSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElasticsearchSampleApplication.class, args);
    }

}
